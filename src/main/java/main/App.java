package main;

import org.apache.velocity.app.VelocityEngine;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import spark.utils.IOUtils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;
import java.io.InputStream;
import java.security.SecureRandom;
import java.util.HashMap;

import static spark.Spark.get;
import static spark.Spark.post;

public class App {
    public static void main(String[] args) {
        VelocityEngine configuredEngine = new VelocityEngine();
        configuredEngine.setProperty("runtime.references.strict", true);
        configuredEngine.setProperty("resource.loader", "class");
        configuredEngine.setProperty("class.resource.loader.class",
                "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityTemplateEngine velocityTemplateEngine = new
                VelocityTemplateEngine(configuredEngine);


        get("/chiper", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String plain = req.queryParamOrDefault("plain", "");
            Cipher cipher = null;

            cipher = Cipher.getInstance("AES");
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            SecureRandom secureRandom = new SecureRandom();
            int keyBitSize = 256;
            keyGenerator.init(keyBitSize, secureRandom);
            SecretKey secretKey = keyGenerator.generateKey();
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            Cipher cipher1 = cipher;

            if(!plain.isEmpty()){
                byte[] plainText = plain.getBytes("UTF-8");
                byte[] cipherText = cipher1.doFinal(plainText);
                plain = new String(cipherText);
                model.put("enkrip",plain);
            }else {
                model.put("enkrip", "");
            }
            String templatePath = "/views/chiper.vm";
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

        get("/chiperFile", (req, res) -> {
            HashMap<String, Object> model = new HashMap<>();
            String templatePath = "/views/chiper_file.vm";
            String chiper = req.queryParamOrDefault("chiper", "");
            model.put("enkrip", chiper);
            return velocityTemplateEngine.render(new ModelAndView(model,
                    templatePath));
        });

        post("/chiperFile", (req, res) -> {
            String chiperRes = null;
            try {
                req.attribute("org.eclipse.jetty.multipartConfig", new
                        MultipartConfigElement(System.getProperty("user.dir")+"//tmp"));
                Part filePart = req.raw().getPart("myfile");
                if (filePart != null) {
                    try (InputStream inputStream = filePart.getInputStream()) {
                        Cipher cipher = null;

                        cipher = Cipher.getInstance("AES");
                        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
                        SecureRandom secureRandom = new SecureRandom();
                        int keyBitSize = 256;
                        keyGenerator.init(keyBitSize, secureRandom);
                        SecretKey secretKey = keyGenerator.generateKey();
                        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
                        byte[] cipherText = cipher.doFinal(IOUtils.toByteArray(inputStream));
                        chiperRes = new String(cipherText);

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            res.redirect("/chiperFile?chiper="+chiperRes);
            return res;
        });


    }
}
